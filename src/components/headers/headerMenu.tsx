import { Link } from "react-router-dom";
const HeaderMenu = () => {
    return <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
            <Link className="navbar-brand" to={'/'}>Navbar</Link>

            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link active" to={'/login'}>Войти</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link active" to={'/register'}>Регистрация</Link>
                </li>
            </ul>
        </div>
    </nav>
};
export default HeaderMenu;