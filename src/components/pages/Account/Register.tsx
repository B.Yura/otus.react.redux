import { User } from "../../../models/user";
import { useDispatch } from "react-redux";
import { Actions } from "../../../stateManagment/userReducer";
import React, { useState } from "react";
import Login from "./Login";
import ChkRedux from "./ChkRedux";
let user: User = {
    login: "",
    password: ""
};

const Register = () => {
    const dispatch = useDispatch();

    const saveUser = () => {
        dispatch(Actions.setUser(user));
    };

    const [user, setData] = useState({ login: "", password: "" });
    const handleChange = (e: any) => setData({ ...user, [e.target.name]: e.target.value });

    return <>
        <div className="container mt-5" >

            <div className="row justify-content-center mb-3">
                <input type="text" placeholder="Введите login" name="login" className="form-control w-50" value={user.login} onChange={e => handleChange(e)} />
            </div>
            <div className="row justify-content-center mb-3">
                <input type="password" placeholder="Введите пароль" name="password" className="form-control w-50" value={user.password} onChange={e => handleChange(e)} />
            </div>
            <div className="row justify-content-center mb-3">
                <button onClick={saveUser} className="btn btn-primary w-50">Зарегистрироваться</button>
            </div>
            <ChkRedux />
        </div >
        
    </>
};
export default Register;