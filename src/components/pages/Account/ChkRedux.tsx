import { connect, useSelector } from "react-redux";

const ChkRedux = () => {
    const user = useSelector((x: any) => x.user);
    return (

        <div className="row">
            <h1>{JSON.stringify(user)}</h1>
        </div>
    );
};

export default ChkRedux;