import React, { useState } from "react";
import { User } from "../../../models/user";
let user: User = {
    login: "",
    password: ""
};
const Login = () => {

    const [user, setData] = useState({ login: "", password: "" });
    const handleChange = (e: any) => setData({ ...user, [e.target.name]: e.target.value });

    return <>
        <div className="container mt-5" >

            <div className="row justify-content-center mb-3">
                <input type="text" placeholder="Введите login" name="login" className="form-control w-50" value={user.login} onChange={e => handleChange(e)} />
            </div>
            <div className="row justify-content-center mb-3">
                <input type="password" placeholder="Введите пароль"  name="password" className="form-control w-50" value={user.password} onChange={e => handleChange(e)} />
            </div>
            <div className="row justify-content-center mb-3">
                <button  className="btn btn-primary w-50">Войти</button>
            </div>

        </div >
    </>

}
export default Login;