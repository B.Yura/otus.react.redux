
import './App.css';
import { Route, Link, Routes, BrowserRouter } from "react-router-dom";
import HomePage from './components/pages/Home/HomePage';
import Error404 from './components/pages/Errors/Error404';
import Login from './components/pages/Account/Login';
import Register from './components/pages/Account/Register';
import HeaderMenu from './components/headers/headerMenu';

function App() {
  return (

    <BrowserRouter>
      <HeaderMenu />
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="*" element={<Error404 />} />
      </Routes>
    </BrowserRouter >
  );
}

export default App;
