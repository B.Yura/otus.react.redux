import userReducer from "./stateManagment/userReducer";
import { combineReducers, createStore } from "redux";
// redux
const rootReducer = combineReducers({
    user: userReducer,
});

const store = createStore(rootReducer);

export default store;