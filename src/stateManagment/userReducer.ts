import { User } from "../models/user";

export const OrderState = Object.freeze({
    REGISTER_USER: '[USER_STATE] REGISTER_USER',
    LOGIN_USER: '[USER_STATE] LOGIN_USER',
});

interface State {
    user: User | null;
}

const initialState: State = {
    user: null
}


export const Actions = Object.freeze({
    setUser: (user: User) => ({ type: OrderState.REGISTER_USER, payload: user })
});


const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case OrderState.REGISTER_USER:
    
            return { ...state, user: action.payload };

        default:
            return state;
    }
};

export default userReducer;